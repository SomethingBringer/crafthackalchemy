package android.crafthack.alchemy.Database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Recipe")
data class Recipe(
    @PrimaryKey(autoGenerate = true) var id: Int,
    @ColumnInfo(name = "Description") var description: String,
    @ColumnInfo(name = "Name") var name: String,
    @ColumnInfo(name = "Img") var img: String
)
