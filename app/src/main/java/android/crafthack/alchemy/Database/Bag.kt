package android.crafthack.alchemy.Database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Component")
data class Component(
    @PrimaryKey(autoGenerate = true) var id : Int,
    @ColumnInfo(name = "Quantity") var quantity: Int,
    @ColumnInfo(name = "Img") var img: String,
    @ColumnInfo(name = "Id") var name: String
)