package android.crafthack.alchemy.Database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase


@Database(entities = arrayOf(Component::class),version = 1)
abstract class BagDatabase: RoomDatabase(){
    abstract val bagDao: BagDao

    companion object{
        @Volatile
        private var INSTANCE: BagDatabase?=null

        fun getInstance(context: Context):BagDatabase {
            var instance = INSTANCE
            if (instance == null) {
                instance = Room.databaseBuilder(
                    context.applicationContext,
                    BagDatabase::class.java,
                    "bag_database"
                )
                    .fallbackToDestructiveMigration()
                    .build()
                INSTANCE = instance
            }
            return instance
        }
    }
}

