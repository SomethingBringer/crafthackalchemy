package android.crafthack.alchemy.Database

import androidx.room.Dao
import androidx.room.Query

@Dao
interface BagDao{
    @Query("Select * from Component")
    fun getAll(): List<Component>

    @Query("Delete from Component")
    fun deleteAll()

    @Query("Select * from Component where Id like:Id")
    fun getById(Id: Int): Component

    @Query("Update Component set Quantity=(Select quantity from Component where Id like:Id)+1 where Id like:Id")
    fun incrementComponent(Id:Int)

    @Query("Update Component set Quantity=(Select quantity from Component where Id like:Id)-1 where Id like:Id")
    fun decrementComponent(Id:Int)
}