package android.crafthack.alchemy.Database

import androidx.room.Dao
import androidx.room.Query

@Dao
interface RecipesBookDao{
    @Query("Select * from Recipe")
    fun getAll(): List<Component>

    @Query("Delete from Recipe")
    fun deleteAll()

    @Query("Select * from Recipe where Id like:Id")
    fun getById(Id: Int): Component

}