package android.crafthack.alchemy

import android.crafthack.alchemy.Database.Component
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

class InventoryAdapter() : RecyclerView.Adapter<InventoryAdapter.ViewHolder>(){
    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.findViewById(R.id.txtComponentName)
        val quantity: TextView = itemView.findViewById(R.id.txtQuantity)
        val img: ImageView = itemView.findViewById(R.id.imgComponent)
    }
    var data = listOf<Component>()
    set(value) {
        field = value
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater
            .inflate(R.layout.fragment_inventory_fragment,parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = data[position]
        val res = holder.itemView.context.resources
        holder.name.text = data[position].name
        holder.quantity.text = "Количество: "+data[position].quantity.toString()
        Glide.with(holder.img.context)
            .load(data[position].img)
            .into(holder.img)
    }
}