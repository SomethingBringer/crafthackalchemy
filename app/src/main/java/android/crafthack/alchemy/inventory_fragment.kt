package android.crafthack.alchemy


import android.crafthack.alchemy.Database.BagDao
import android.crafthack.alchemy.Database.BagDatabase
import android.crafthack.alchemy.databinding.FragmentInventoryFragmentBinding
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class inventory_fragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding:FragmentInventoryFragmentBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_inventory_fragment, container, false)

        val adapter = InventoryAdapter()
        binding.invertoryMain.adapter=adapter
        val application = requireNotNull(this.activity).application
        val datasource = BagDatabase.getInstance(application).bagDao
        adapter.data = datasource.getAll()
        return binding.root
    }


}
